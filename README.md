### Fail2SQL v1.3


The original version of Fail2SQL used deprecated versions of PHP's MySQL driver as well as MaxMind's Free GeoIP teir. I rewrote it to make use of PDO, PHP8+, and IP2Locations Free databases.

## Installation


1. Create a MySQL database called fail2ban
2. Create fail2ban MySQL user to access fail2ban database (needs INSERT, UPDATE, DELETE)
3. Create table by piping fail2ban.sql into mysql (mysql -u fail2ban -p fail2ban < fail2ban.sql)
4. Edit fail2sql and change home path and sql login details at the top of the file.
5. Update Geo IP Database (./fail2ban -u)
6. Tell fail2ban to call fail2sql by appending to actionban in your action script.

Example for /etc/fail2ban/action.d/iptables.conf

actionban = iptables -I fail2ban-<name> 1 -s <ip> -j DROP
            /usr/local/fail2sql/fail2sql -i <name> <protocol> <port> <ip> <failures> <fq-hostname>

## Usage

fail2sql [-h|-l|-c|-u]
	-h: The help page
	-l: List entries in the database (max 50 showed)
	-c: Clear the database and start fresh
	-u: Update GeoIP database (downloads from maxmind)


### Original Author

Jordan Tomkinson <jordan@moodle.com> 

https://fail2sql.sourceforge.net/





