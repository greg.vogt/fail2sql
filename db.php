<?php

// Shamelessly stolen from myself:
// https://gitlab.com/greg.vogt/vega

#use PDO;

/**
 * [Description Database]
 */
class Database
{

    // Database Parameters
    private $host;
    private $db_name;
    private $username;
    private $password;
    private $handler;
    private $conn;

    public function __construct($host, $db_name, $username, $password)
    {
        //$this->handler = new Error_Handler();

        $this->host = $host;
        $this->db_name = $db_name;
        $this->username = $username;
        $this->password = $password;

        $this->connect();
    }
    public function __destruct()
    {
        $this->handler = null;
    }

    // Database Connection
    /**
     * @return [type]
     */
    public function connect()
    {
        // Close connection in case one remains open
        $this->conn = null;
        // Attempt to build our database connection
        try {
            $this->conn = new PDO('mysql:host=' . $this->host . ';dbname=' . $this->db_name, $this->username, $this->password, array(
                PDO::MYSQL_ATTR_LOCAL_INFILE => true,
            ));
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            //$this->conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, FALSE);
        } catch (\PDOException $e) {
            echo 'Connection Error: ' . $e->getMessage();
        }
        return $this->conn;
    }

    /**
     * @param string $table
     * @param array $callback
     * @param mixed $callback_args
     *
     * @return [type]
     */
    public function checkTable(string $table, array $callback = null, $callback_args = null)
    {
        if ($table == null) {
            throw new Exception("Function checkTable expects two variables!");
        }

        // Check if table exists
        try {
            $res = $this->pdoQueryGeneral("SELECT * FROM " . $table . " LIMIT 1");
            // If an array is returned; the table exists
            if (is_array($res)) {
                return true;
            }
        } catch (\Exception $e) {
            if (trim(explode(":", $e->getMessage())[1]) === "SQLSTATE[42S02]") {
                try {
                    if ($callback != null) {
                        (empty($callback_args) ? $callback() : $callback($callback_args));
                    }
                    return false;
                } catch (Exception $e) {
                    throw new \Exception("Unable to confirm status of users table");
                }
            } else {
                throw new \Exception("Unknown PDO Error: " . $e->getMessage());
            }
        }
    }

    /**
     * @param mixed $conn
     * @param mixed $query
     * @param mixed $binds
     *
     * @return [type]
     */
    public function pdoQueryExist($conn, $query, $binds)
    {
        try {
            // Prepare Statement
            $stmt = $this->conn->prepare($query);
            // Bind params
            foreach ($binds as $key => $value) {
                $stmt->bindValue($key, $value);
            }
            // Execute Query
            $stmt->execute();
            if ($stmt->rowCount() !== 0) {
                return true;
            } else {
                return false;
            }
        } catch (\PDOException $e) {
            $this->handler->pdoErrorParse($e);
        }
    }

    /**
     * @param mixed $query
     * @param array $binds
     *
     * @return [type]
     */
    public function pdoQueryGeneral($query, $binds = array())
    {
        $queryType = explode(" ", $query);

        try {
            // Prepare Statement
            $stmt = $this->conn->prepare($query);

            // Bind params
            foreach ($binds as $key => $value) {
                if (is_numeric($value)) {
                    $stmt->bindValue($key, $value, PDO::PARAM_INT);
                } else {
                    $stmt->bindValue($key, $value);
                }
            }
            
            //var_dump($stmt->queryString);

            // Execute Query
            if ($stmt->execute()) {
                if ($queryType[0] == "SELECT") {
                    return $stmt->fetchAll(PDO::FETCH_ASSOC);
                } elseif ($queryType[0] == "INSERT") {
                    return $this->conn->lastInsertId();
                } elseif ($queryType[0] == "UPDATE") {
                    return true;
                } else {
                    // Return true if statment executes successfully if we dont know
                    // what it is.
                    return true;
                }
            } else {
                return false;
            }
        } catch (\PDOException $e) {
            throw new \Exception($e);
            //var_dump($e);
        }
    }
}
